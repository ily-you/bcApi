<?php

namespace App\Controller;

use App\Entity\Listings;
use App\Repository\GarageRepository;
use App\Repository\ListingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListingController extends AbstractController
{
    /**
     * @Route("/listing", name="listing")
     */
    public function index(): Response
    {
        return $this->render('listing/index.html.twig', [
            'controller_name' => 'ListingController',
        ]);
    }
        /**
     * @Route("/add_listing/{id}", name="listing_add")
     * @param Request $request
     */
    public function add($id, GarageRepository $garageRepository, Request $request, EntityManagerInterface $entityManager)
    {
        $garage = $garageRepository->find($id);
        if ($request->getMethod() === 'POST') {
            $listing = new Listings();
            $listing->setTitle($request->request->get('title'));
            $listing->setReleaseYear($request->request->get('year'));
            $listing->setKm($request->request->get('km'));
            $listing->setPrice($request->request->get('price'));
            $listing->setBrand($request->request->get('brand'));
            $listing->setModel($request->request->get('model'));
            $listing->setDescription($request->request->get('description'));
            $listing->setFuel($request->request->get('fuel'));
            $listing->setPicture($request->request->get('picture'));
            $listing->setGarage($garage);

            $entityManager->persist($listing);
            $entityManager->flush();
            return $this->redirectToRoute('listing_list');
            
        }
        return $this->render('listing/add.html.twig', ['garage' => $garage]);
        
    }
      /**
     *@Route("/", name="listing_list")
     */
    public function liste(ListingsRepository $listingsRepository): Response
    {
        $listings =  $listingsRepository->findAll();
        return $this->render('listing/list.html.twig', ['listings' => $listings]);

    }
}
