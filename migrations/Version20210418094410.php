<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210418094410 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE garage ADD listings VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE garage RENAME INDEX idx_9f26610b7e3c61f9 TO IDX_9F26610BA76ED395');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE garage DROP listings');
        $this->addSql('ALTER TABLE garage RENAME INDEX idx_9f26610ba76ed395 TO IDX_9F26610B7E3C61F9');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677 ON `user`');
    }
}
